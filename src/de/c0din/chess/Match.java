package de.c0din.chess;

import org.bukkit.entity.Player;

/**
 * Created by C0din on 18.12.2016.
 * https://c0din.de
 */
public class Match {

    private Player white;
    private Player black;

    public Match(Player white, Player black) {
        this.white = white;
        this.black = black;
    }
}
