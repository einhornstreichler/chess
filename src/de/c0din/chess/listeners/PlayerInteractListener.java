package de.c0din.chess.listeners;

import de.c0din.chess.Armor;
import de.c0din.chess.Chessmen;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

/**
 * Created by C0din on 18.12.2016.
 * https://c0din.de
 */
public class PlayerInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent event) {
        System.out.println("Type: " + event.getRightClicked().getType());
        System.out.println("ID: " + event.getRightClicked().getEntityId());
        System.out.println("Name: " + event.getRightClicked().getName());
        Entity entity = event.getRightClicked();
        if(!entity.getType().equals(EntityType.ARMOR_STAND)) {
            return;
        }
        event.setCancelled(true);
        ArmorStand armorStand = (ArmorStand) entity;
        Armor armor = Armor.getArmor(armorStand);
        if(armor.equals(Chessmen.BLACK_KING.getArmor())) {

        }
    }
}
