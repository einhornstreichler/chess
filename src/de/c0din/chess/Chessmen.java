package de.c0din.chess;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Created by C0din on 03.01.2017.
 * https://c0din.de
 */
public enum Chessmen {

    BLACK_KING(new Armor(new ItemStack(Material.AIR),new ItemStack(Material.AIR),new ItemStack(Material.AIR),new ItemStack(Material.AIR)));

    final Armor armor;

    Chessmen(Armor armor) {
        this.armor = armor;
    }

    public Armor getArmor() {
        return armor;
    }
}
