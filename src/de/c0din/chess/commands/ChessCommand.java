package de.c0din.chess.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by C0din on 22.12.2016.
 * https://c0din.de
 */
public class ChessCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(args.length != 2) {
            // TODO
            return true;
        }
        if(!(sender instanceof Player)) {
            sender.sendMessage("This command is only for Players");
            return true;
        }
        Player player = (Player) sender;
        if(args[0].equalsIgnoreCase("white") || args[0].equalsIgnoreCase("black")) {
            switch(args[1].toLowerCase()) {
                case "king":
                    break;
                case "queen":
                    break;
                case "bishop":
                    break;
                case "knight":
                    break;
                case "rook":
                    break;
                case "pawn":
                    break;
                default:
                    player.sendMessage("Unknown chessman: " + args[1]);
            }
        }
        return false;
    }
}
