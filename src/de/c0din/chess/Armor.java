package de.c0din.chess;

import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;

/**
 * Created by C0din on 03.01.2017.
 * https://c0din.de
 */
public class Armor {

    private ItemStack helmet;
    private ItemStack chestplate;
    private ItemStack leggings;
    private ItemStack boots;

    public Armor(ItemStack helmet, ItemStack chestplate, ItemStack leggings, ItemStack boots) {
        this.helmet = helmet;
        this.chestplate = chestplate;
        this.leggings = leggings;
        this.boots = boots;
    }

    public ItemStack getHelmet() {
        return helmet;
    }

    public ItemStack getChestplate() {
        return chestplate;
    }

    public ItemStack getLeggings() {
        return leggings;
    }

    public ItemStack getBoots() {
        return boots;
    }

    public static Armor getArmor(ArmorStand armorStand) {
        return new Armor(armorStand.getHelmet(), armorStand.getChestplate(),
                armorStand.getLeggings(), armorStand.getBoots());
    }
}
