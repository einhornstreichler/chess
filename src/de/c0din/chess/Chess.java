package de.c0din.chess;

import de.c0din.chess.listeners.PlayerInteractListener;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * Created by C0din on 18.12.2016.
 * https://c0din.de
 */
public class Chess extends JavaPlugin {

    private FileConfiguration languageConfig;

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(new PlayerInteractListener(), this);
        languageConfig = YamlConfiguration.loadConfiguration(
                new File(getConfig().getString("language") + ".yml"));
    }

    @Override
    public void onDisable() {

    }
}
